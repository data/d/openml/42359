# OpenML dataset: CSM

https://www.openml.org/d/42359

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Mehreen Ahmed  
**Source**: [original](http://archive.ics.uci.edu/ml/datasets/CSM+(Conventional+and+Social+Media+Movies)+Dataset+2014+and+2015#) - Date unknown  
**Please cite**:   

Conventional and Social Media Movies (CSM) - Dataset 2014 and 2015 Data Set

12 features categorized as conventional and social media features. Both conventional features, collected from movies databases on Web as well as social media features(YouTube,Twitter).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42359) of an [OpenML dataset](https://www.openml.org/d/42359). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42359/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42359/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42359/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

